<?php

use App\Http\Controllers\PayController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.main');
});

Route::get('/index',[PayController::class ,'index'])->name('pays.index');
Route::get('/ajouter',[PayController::class ,'ajouter'])->name('pays.ajouter');
Route::post('/pays',[PayController::class ,'store'])->name('pays.store');
