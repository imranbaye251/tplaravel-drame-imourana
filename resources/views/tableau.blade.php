@extends('layout.main');
@section('content');
<div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title ">Simple Table</h4>
          <p class="card-category"> Here is a subtitle for this table</p>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table">
              <thead class=" text-primary">
                <th>
                  ID
                </th>
                <th>
                  Libelle
                </th>
                <th>
                  Description
                </th>
                <th>
                  Code_indicatif
                </th>
                <th>
                  Continent
                </th>
                <th>
                Population
                </th>
                <th>
                Capitale
                </th>
                <th>
                Monnaie
                </th>
                <th>
                Langue
                </th>
                <th>
                Superficie
                </th>
                <th>
                Est_laique
                </th>
              </thead>
              <tbody>
                  @foreach ($pays as $pat)
                <tr>
                    <td>{{$pat ->id}}</td>
                    <td>{{$pat ->libelle}}</td>
                    <td>{{$pat ->description}}</td>
                    <td>{{$pat ->code_indicatif}}</td>
                    <td>{{$pat ->continent}}</td>
                    <td>{{$pat ->population}}</td>
                    <td>{{$pat ->capitale}}</td>
                    <td>{{$pat ->monnaie}}</td>
                    <td>{{$pat ->langue}}</td>
                    <td>{{$pat ->superficie}}</td>
                    <td>{{$pat ->est_laique}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
